// mi9.js
// JSON request/response server
// Brief found at: https://mi9-coding-challenge.herokuapp.com/
// Live app deploy at http://mi9-limmer.herokuapp.com
// Written by James Limmer
// Nov 20th 2014

var http = require('http');
var jsonParser = require('./lib/jsonParser');

// Create the web server
var server = http.createServer(function(req,res){
	// Look at headers
	var headerObj = req.headers;
	// store the request data in new variable
	var jsonBody = "";
	req.on('data', function(chunk){
		jsonBody += chunk;
		// console.log(JSON.stringify(jsonBody));
	});
	// On request end, parse the JSON and send a response.
	req.on('end', function(){
		// console.log(jsonBody);
		jsonParser.parseMe(jsonBody, req, function(result,err){
			if(err){
				console.log('Error occurred: '+JSON.stringify(err));
				// send error
				res.setHeader('Content-Type', 'application/json');
				res.statusCode = 400;
				res.end(err);
			} else {
				//console.log('SENDING AS STRING:');
				//console.log(JSON.stringify(result));
				// send result
				res.setHeader('Content-Type', 'application/json');
				res.statusCode = 200;
				res.end(JSON.stringify(result));
			};
		});
	});
});
var port = process.env.PORT || 5000;
server.listen(port);
console.log('JSON server is running...'+port);