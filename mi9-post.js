// mi9-post.js
// Test server to get Example JSON data and post it to the server.

var jsonParser = require('./lib/jsonParser');
var http = require('http');
var fs = require('fs');
// which json file to parse (local only)
var filename = process.argv[2];
// good or bad JSON (argument is either 'good' or 'bad' or 'superBad')

fs.readFile(filename, 'utf8', function(err,data){
	if(err){
		console.log(err);
		throw err;
	};
	console.log('Read: ' + filename);

	var options = {
		host: 'localhost',
		port: 5000,
		path: '/',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Content-Length': Buffer.byteLength(data)
		}
	};

	var req = http.request(options, function(res){
		res.setEncoding('utf8');
		res.on('data', function(chunk){
			console.log('Response: ' + JSON.stringify(JSON.parse(chunk)));
			jsonParser.testResponse(chunk);
		});
	});
	req.write(data);
	req.end();
});
