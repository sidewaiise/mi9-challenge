**Mi9 Server Challenge**

**Overview**

This server was written for the Mi9 challenge. The code was written in a fairly vanilla format, using minimum npm modules to try and keep the application light-weight.

**Usage Instructions**

SERVER-SIDE

To start the server `forever` should be installed, and use the command

`forever -m 5 mi9.js`

or since we deployed on Heroku in this case, the Procfile looks like:

`web: ./node_modules/.bin/forever -m 5 mi9.js`

This sets the application running on the server in background, and allows restarting of the application on Heroku if the process terminates.

TESTING

There is a testing app called `mi9-post.js` in the same root folder as `mi9.js`.

This testing app is designed to send the example JSON data provided by Mi9 team to the running server @ http://mi9-limmer.herokuapp.com/. The testing script is designed to be run as a once-off, and should return command-line test notifiers as 'SUCCESS' or 'FAIL'.

The `mi9-post-live` application runs a number of tests

- Response content-type is application/json
- Return JSON has `response` attribute
- response array items include child attributes: image, slug, title
- Tests for error handling
  - Posts alternative content in the form of base64:gif dataURL (response should be error) but with application/json content-header

Usage: `node mi9-post-live ./post-data/datauri.txt`
- returns error response

Usage: `node mi9-post-live ./post-data/sample_request.json`
- returns SUCCESS response

**Libraries**

Libraries can be found in the ./lib folder.

jsonParser.js

 - parseMe(data, req, cb)
 - we pass a JSON string and the request object to this function, and use a callback to determine result or error.

EXAMPLE:

    parseMe(jsonStringData, req, function(result, err){
        if(err){
            // Do something with Error (err) response
        } else {
            // Do something with Result (result) response (JSON string)
        }
    });

The result (if there is one) will be in the format:

    {
        response: [
            {
                "image": "http://imagelocation.com/path/to/image.jpg",
                "slug": "something/ch9",
                "title": "Title"
            },
            {.....}
        ]
    }

 - testResponse(responseData)
 - By varying the responseData we test the resiliance of the code, quality of the (valid) response data and error handling.

EXAMPLE:

    testResponse(responseData);
     --> console output tells of SUCCESS or points of FAILURE.

 errorHandlers.js

 contains the error responses (in JSON) for handling different error events. In this case, we only have one type of error, called by the `standard` function.

 EXAMPLE:

    var errors = require('./lib/errorResponses');
    var errResponse = errors.standard();
    console.log(errResponse);

Returns `{ "error": "Could not decode request" }`;