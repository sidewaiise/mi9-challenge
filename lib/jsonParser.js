// jsonParser.js
// Takes Json data and checks to make sure it meets the rules.
// Rules: - JSON request must include
//         -- drm == true
//         -- episodeCount > 0
//        - Response must include:
//         -- image
//         -- slug
//         -- title
//        - Error handling for invalid JSON
//         -- 'Could not decode request'
//         -- Error code 400

var errors = require('../lib/errorHandlers');
var http = require('http');

exports.parseMe = function(data, req, callback){
	var result={"response": [] }, skippedItems={"skipped":[]};
	var dataType = req.headers['content-type'];
	// First, make sure the content-type is JSON.
	if(dataType == 'application/json'){
		// Second, if JSON can parse the data.
		// Using try so the application doesn't crash if the wrong datatype is sent to parse.
		try {
			var dataObject = JSON.parse(data);
		} catch(e){
			var dataObject = 'fail';
		} finally {
			if(dataObject == 'fail'){
				console.log(errors.standard);
				return callback(result, JSON.stringify(errors.standard()));
			} else {
				if(dataObject != undefined){
					// Then, payload must exist
					if(!dataObject.payload){
						// if there's no payload attribute then immediate error.
						return callback(result, errors.standard());
					} else {
						var resArray = dataObject.payload
						// loop through the payload items
						for(var i=0;i<=resArray.length;i++){
							// adhere to the rules. In some cases resArray[i] is undefined, so we need to inlcude this in the if statement.
							if(resArray[i] != undefined && resArray[i].drm == true && resArray[i].episodeCount > 0){
								// Check the specified response keys are available
								if(resArray[i].image.showImage && resArray[i].slug && resArray[i].title){
									// define response result
									var resGroup = [{ "image": resArray[i].image.showImage, "slug": resArray[i].slug, "title": resArray[i].title }];
									// send group to response array
									// console.log(resGroup);
									result.response = result.response.concat(resGroup);
								} else {
									return callback(result, errors.standard());
								}
								
							} else {
								// Keep track of the items that are skipped.
								if(resArray[i] != undefined && resArray[i].title){
									skippedItems.skipped = skippedItems.skipped.concat([{"title": resArray[i].title}]);
									//console.log('Entry Skipped: '+ resArray[i].title);
								};
							};
						};
						console.log(result);
						console.log(skippedItems);
						// console.log("Items: " + dataObject.payload.length);
					}

				} else {
					// callback the error response
					return callback(result, errors.standard());
				};
			}
		};
		
		return callback(result);
	}
};
	
exports.testResponse = function(responseData){
	var data = JSON.parse(responseData);
	if(data.response){
		console.log('Response SUCCESS');
		for(var i=0;i<=data.response.length;i++){
			//console.log(data.response[i]);
			for(var key in data.response[i]){
				// console.log(key);
				if(key == 'image' || key == 'slug' || key == 'title'){
					console.log(key + ' Response SUCCESS');
				} else {
					console.log(key + ' Invalid FAIL');
				};
			};
		};
	} else {
		console.log('Response not included. FAIL');
	}
	
};
	
	
