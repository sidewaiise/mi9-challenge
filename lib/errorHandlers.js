// errorHandlers.js
// Just a function to minimise the amount of Error code to write.
// Can be used to specify a number of error returns. In this case we are only using one.

exports.standard = function(){
	var error = { "error": "Could not decode request" };
	return error;
};